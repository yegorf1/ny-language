#pragma once
#include <queue>
#include <stack>
#include <string>
#include <stdio.h>

#define WINDOWS


#define NL_SYNTAX_UNEXPECTED_SYMBOL 1


#define NL_SYMBOL_DEFENITION "def"
#define NL_NUM_DEFENNITION 1
#define NL_SYMBOL_RETURN "return"
#define NL_NUM_RETURN 2
#define NL_SYMBOL_BREAK "break"
#define NL_NUM_BREAK 3
#define NL_SYMBOL_IF "if"
#define NL_NUM_IF 4
#define NL_SYMBOL_ELSE "else"
#define NL_NUM_ELSE 5
#define NL_SYMBOL_WHILE "while"
#define NL_NUM_WHILE 6

#define NL_OPERATOR_ASSIGN "="
#define NL_OPERATOR_ADD "+"
#define NL_OPERATOR_SUB "-"
#define NL_OPERATOR_MULTIPLY "*"
#define NL_OPERATOR_DEVIDE "/"
#define NL_OPERATOR_POW "^"
#define NL_OPERATOR_BIGGER ">"
#define NL_OPERATOR_LESS "<"
#define NL_OPERATOR_EQUALS "=="

#define NL_TYPE_FUNCTION "Function"
#define NL_TYPE_INTEGER "Integer"
#define NL_TYPE_STRING "String"
#define NL_TYPE_BOOLEAN "Boolean"
#define NL_TYPE_ERROR "Error"

using namespace std;

namespace Ny {
	inline bool isstringbegin(char c) {
		return c == '\'' || c == '"';
	}

	inline char spec(char c) {
		switch (c) {
			case 'n':
				return '\n';

			case 'r':
				return '\r';

			case 't':
				return '\t';

			default:
				return c;
		}
	}

	inline bool isnewline(char c) {
		return c == '\n' || c == '\r' || c == 32;
	}

	inline bool isoperator(string s) {
		return	s == NL_OPERATOR_ADD || 
				s == NL_OPERATOR_SUB ||
				s == NL_OPERATOR_MULTIPLY ||
				s == NL_OPERATOR_DEVIDE   ||
				s == NL_OPERATOR_POW ||
				s == NL_OPERATOR_ASSIGN;
	}

	inline bool exists(const std::string& name) {
		FILE *file;
#ifdef WINDOWS
		fopen_s(&file, name.c_str(), "r");
#else
		file = fopen(name.c_str(), "r");
#endif
		if (file) {
			fclose(file);
			return true;
		} else {
			return false;
		}
	}
}
