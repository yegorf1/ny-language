#include <math.h>
#include "NLInteger.h"

namespace Ny {
	//TODO: If n > 2 ^ 64?
	NLObject* NLInteger::Add(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		string a = args[0]->value, b = args[1]->value;

		bool isNegativeResult = false;

		if (isNegative(a)) {
			if (isNegative(b)) {
				a = abs(a);
				b = abs(b);

				isNegativeResult = true;
			} else {
				vector<NLObject*> subArgs(0);

				subArgs.push_back(NLObject::CreateInteger(globals, abs(b)));
				subArgs.push_back(NLObject::CreateInteger(globals, abs(a)));

				return Sub(subArgs);
			}
		} else if (isNegative(b)) {
			vector<NLObject*> subArgs(0);

			subArgs.push_back(NLObject::CreateInteger(globals, abs(a)));
			subArgs.push_back(NLObject::CreateInteger(globals, abs(b)));

			return Sub(subArgs);
		}

		unsigned int n = max(a.size(), b.size()) + 1;

		string c;

		reverse(a.begin(), a.end());
		reverse(b.begin(), b.end());

		for (unsigned int i = 0; i < n; i++) {
			c += "0";
			if (i >= a.size()) {
				a += "0";
			}
			if (i >= b.size()) {
				b += "0";
			}
		}

		char querry = 0;

		for (unsigned int i = 0; i < n; i++) {
			c[i] = a[i] + b[i] + c[i] + querry - '0' * 2;

			querry = 0;

			if (c[i] > '9') {
				querry = (c[i] - '0') / 10;
				c[i] = (c[i] - '0') % 10 + '0';
			}
		}


		while (c.size() > 1 && c[c.size() - 1] == '0') {
			c.resize(c.size() - 1);
		}

		reverse(c.begin(), c.end());

		if (isNegativeResult) {
			c.insert(c.begin(), '-');
		}

		return NLObject::CreateInteger(globals, c);
	}

	NLObject* NLInteger::Multiply(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		string a = args[0]->value, b = args[1]->value;

		bool isNegativeResult = isNegative(a) != isNegative(b);

		a = abs(a);
		b = abs(b);

		unsigned int n = max(a.size(), b.size()) * 2 + 1;

		string c = "0", tmp;

		reverse(a.begin(), a.end());
		reverse(b.begin(), b.end());

		for (unsigned int i = 0; i < n; i++) {
			if (i >= a.size()) {
				a += "0";
			}
			if (i >= b.size()) {
				b += "0";
			}
		}

		char querry = 0;

		for (unsigned int j = 0; j < n; j++) {
			tmp = "";
			for (unsigned int i = 0; i < n; i++) {
				tmp += "0";
			}
			long long powerOfTen = pow(10, j);
			for (unsigned int i = 0; i < n; i++) {
				tmp[i] = powerOfTen * (a[i] - '0') * (b[j] - '0') + tmp[i] + querry;

				querry = 0;

				if (tmp[i] > '9') {
					querry = (tmp[i] - '0') / 10;
					tmp[i] = (tmp[i] - '0') % 10 + '0';
				}
			}

			vector<NLObject*> subArgs(0);
			subArgs.push_back(NLObject::CreateInteger(globals, c));
			subArgs.push_back(NLObject::CreateInteger(globals, tmp));

			c = Add(subArgs)->value;
		}


		while (c.size() > 1 && c[c.size() - 1] == '0') {
			c.resize(c.size() - 1);
		}

		reverse(c.begin(), c.end());

		if (isNegativeResult) {
			c.insert(c.begin(), '-');
		}

		return NLObject::CreateInteger(globals, c);
	}

	NLObject* NLInteger::Sub(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		string a = args[0]->value, b = args[1]->value;

		string c;

		if (!isNegative(a)) {
			if (isNegative(b)) {
				vector<NLObject*> subArgs(0);

				subArgs.push_back(NLObject::CreateInteger(globals, a));
				subArgs.push_back(NLObject::CreateInteger(globals, abs(b)));

				return Add(subArgs);
			}
		}

		if (IsLess(args)->ToCBool()) {
			vector<NLObject*> subArgs(0);

			subArgs.push_back(NLObject::CreateInteger(globals, b));
			subArgs.push_back(NLObject::CreateInteger(globals, a));

			c = Sub(subArgs)->value;

			c.insert(c.begin(), '-');

			return NLObject::CreateInteger(globals, c);
		} else {
			unsigned int n = max(a.size(), b.size()) + 1;


			//Preprocess
			reverse(a.begin(), a.end());
			reverse(b.begin(), b.end());

			for (unsigned int i = 0; i < n; i++) {
				c += "0";
				if (i >= a.size()) {
					a += "0";
				}
				if (i >= b.size()) {
					b += "0";
				}
			}

			//Calculation
			char querry = 0;

			for (unsigned int i = 0; i < n; i++) {
				c[i] = a[i] - b[i] + c[i] - querry;

				querry = 0;

				if (c[i] < '0') {
					querry = (10 + ('0' - c[i])) / 10;
					c[i] = '9' - ('0' - c[i]) % 10 + 1;
				}
			}


			while (c.size() > 1 && c[c.size() - 1] == '0') {
				c.resize(c.size() - 1);
			}

			reverse(c.begin(), c.end());

			return NLObject::CreateInteger(globals, c);
		}
	}


	NLObject* NLInteger::IsBigger(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		if (args.size() < 2) {
			return NLObject::CreateError(globals, "To few arguments.");
		} else if (args.size() > 2) {
			return NLObject::CreateError(globals, "To much arguments.");
		}

		string a = args[0]->value, b = args[1]->value;

		bool bothNegative = false;

		if (isNegative(a)) {
			if (isNegative(b)) {
				bothNegative = true;
			} else {
				return NLObject::CreateBool(globals, false);
			}
		} else {
			if (isNegative(b)) {
				return NLObject::CreateBool(globals, true);
			}
		}

		if (a.size() > b.size()) {
			return NLObject::CreateBool(globals, true);
		} else if (a.size() < b.size()) {
			return NLObject::CreateBool(globals, false);
		}

		if (IsEquals(args)->ToCBool()) {
			return NLObject::CreateBool(globals, false);
		}

		return NLObject::CreateBool(globals, a.compare(b) > 0);
	}

	NLObject* NLInteger::IsEquals(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		if (args.size() < 2) {
			return NLObject::CreateError(globals, "To few arguments.");
		} else if (args.size() > 2) {
			return NLObject::CreateError(globals, "To much arguments.");
		} else {
			return NLObject::CreateBool(globals, args[0]->value.compare(args[1]->value) == 0);
		}
	}

	NLObject* NLInteger::IsLess(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		if (args.size() < 2) {
			return NLObject::CreateError(globals, "To few arguments.");
		} else if (args.size() > 2) {
			return NLObject::CreateError(globals, "To much arguments.");
		} else {
			bool equ = IsEquals(args)->ToCBool(),
				 big = IsBigger(args)->ToCBool();
			return NLObject::CreateBool(globals, !equ && !big);
		}
	}

	NLObject* NLInteger::isNegative(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		if (args.size() < 1) {
			return NLObject::CreateError(globals, "To few arguments.");
		} else if (args.size() > 1) {
			return NLObject::CreateError(globals, "To much arguments.");
		} else {
			return NLObject::CreateBool(globals, isNegative(args[0]->value));
		}
	}

	NLObject* NLInteger::abs(vector<NLObject*> args) {
		NLObject* globals = args[0]->globals;

		if (args.size() < 1) {
			return NLObject::CreateError(globals, "To few arguments.");
		} else if (args.size() > 1) {
			return NLObject::CreateError(globals, "To much arguments.");
		} else {
			return NLObject::CreateInteger(globals, abs(args[0]->value));
		}
	}

	bool NLInteger::isNegative(string value) {
		return value.size() > 1 && value[0] == '-';
	}

	string NLInteger::abs(string value) {
		if (isNegative(value)) {
			value.erase(value.begin());
		}

		return value;
	}
}
