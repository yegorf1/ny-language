#pragma once
#include "NLInterpriterGlobals.h"
#include "NLObject.h"
#include "NLNode.h"
#include "NL.h"
#include <vector>
#include <string>
#include <map>

using namespace std;


namespace Ny {
	class NLObject {
	public:

		typedef NLObject* (*NLBind)(vector<NLObject*>);

		//Function
		NLBind bind;
		bool isFunc, isFunctionBinded;
		NLNode functionDefenition;

		NLObject* globals;

		string value, type;

		NLObject();
		NLObject(NLObject* globals);
		NLObject(NLObject* globals, bool isFunc);
		~NLObject();

		static NLObject* CreateClass(NLObject* globals, string name, vector<string> parent);
		static NLObject* CreateFunction(NLObject* globals, NLNode defenition);
		static NLObject* CreateFunction(NLObject* globals, NLBind bind, string name);
		static NLObject* CreateInteger(NLObject* globals, string integer);
		static NLObject* CreateError(NLObject* globals, string error);
		static NLObject* CreateString(NLObject* globals, string str);
		static NLObject* CreateBool(NLObject* globals, bool boolean);

		NLObject* PutNone(string name);

		bool IsVariableExists(string name);
		bool IsObjectExists(string name);
		bool IsFunctionExists(string name);

		inline bool ToCBool() {
			if (type == NL_TYPE_BOOLEAN && value.size()) {
				return value[0] == 'T' || value[0] == 't';
			} else if (type == NL_TYPE_INTEGER) {
				return value.compare("0") != 0;
			} else if (type == NL_TYPE_STRING) {
				return value.size() > 0;
			} else {
				return value != "None";
			}
		}

		inline NLObject* CallBinded(vector<NLObject*> args) {
			if (isFunc && isFunctionBinded) {
				return bind(args);
			}

			return new NLObject();
		}
#ifdef WINDOWS
		(NLObject*)& operator[](string i) {
			fields["type"] = CreateString(globals, type);
			if (IsVariableExists(i)) {
				fields[i]->fields["name"] = CreateString(globals, i);
			}

			return fields[i];
		}
#else
		NLObject*& operator[](string i) {
			fields["type"] = CreateString(globals, type);
			if (IsVariableExists(i)) {
				fields[i]->fields["name"] = CreateString(globals, i);
			}

			return fields[i];
		}
#endif

	private:
		map<string, NLObject*> fields;
	};
}

