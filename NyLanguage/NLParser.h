#pragma once
#include <vector>
#include <string>

#include "NL.h"
#include "NLToken.h"

namespace Ny {
	class NLParser {
	private:
		string file;

		string ReadString(string &line, unsigned int &begin);

	public:
		NLParser(const char* filename);
		~NLParser();

		int GetTokens(vector<NLToken> &tokens);
	};
}

