#include "NLInterpritator.h"
#include "NLSyntaxTreeBuilder.h"
#include "NLInterpriterGlobals.h"
#include "NLDefaults.h"
#include "NLParser.h"
#include "NLBinds.h"
#include "NLObject.h"
#include <iostream>

namespace Ny {
	NLObject classes, globals, *currentEnviroment;

	NLInterpritator::NLInterpritator() {
		showTree = false;

		currentEnviroment = &globals;

		NLDefaults::CreateDefaults(globals, classes);
	}

	NLInterpritator::~NLInterpritator() {}

	int NLInterpritator::Run(const char* filename) {
		NLParser* parser = new NLParser(filename);

		vector<NLToken> tokens(0);
		parser->GetTokens(tokens);

		NLSyntaxTreeBuilder* syntaxTreeBuilder = new NLSyntaxTreeBuilder(tokens);
		syntaxTreeBuilder->GenerateTree();

		if (showTree) {
			syntaxTreeBuilder->program.PrintToCout(0);
		}

		RunTree(syntaxTreeBuilder->program);

		return 0;
	}

	void NLInterpritator::RunTree(NLNode program) {
		RunNode(&program);
	}

	NLObject* NLInterpritator::RunNode(NLNode* node) {
		returned = false;
		switch (node->type) {
			case NLNode::List:
			case NLNode::Program: {
				for (unsigned int i = 0; i < node->childs.size(); i++) {
					RunNode(node->childs[i]);
				}
				break;
			}

			case NLNode::Operator: {
				return Operator(node);
			}

			case NLNode::Value: {
				if (node->subtype == NL_TYPE_INTEGER) {
					return NLObject::CreateInteger(&globals, node->value);
				} else if (node->subtype == NL_TYPE_STRING) {
					return NLObject::CreateString(&globals, node->value);
				} else {
					//TODO: Error
				}
				break;
			}

			case NLNode::Call:
				return CallFunction(node);

			case NLNode::Identifer:{
				string name = GetName(node);
				if (currentEnviroment->IsObjectExists(name)) {
					return currentEnviroment->operator[](name);
				} else if (globals.IsObjectExists(name)) {
					return globals[name];
				} else {
					//TODO: Error
				}
				break;
			}

			case NLNode::Defenition: {
				string name = GetName(node);

				if (currentEnviroment->IsObjectExists(name)) {
					return NLObject::CreateError(&globals, "Variable already exists(" + name + ")");
				}

				currentEnviroment->PutNone(name);

				return currentEnviroment->operator[](name);
			}

			case NLNode::FunctionDefenition: {
				string name = GetName(node);

				if (currentEnviroment->IsFunctionExists(name)) {
					return NLObject::CreateError(&globals, "Function already exists(" + name + ")");
				}

				return currentEnviroment->operator[](name) = NLObject::CreateFunction(&globals, *node);
			}

			case NLNode::Return: {
				NLObject* res = RunNode(node->childs[0]);
				returned = true;
				return res;
			}

			case NLNode::If: {
				return IfStatement(node);
			}
			
			case NLNode::While: {
				return WhileStatement(node);
			}

			default:
				break;
		}

		return new NLObject();
	}

	NLObject* NLInterpritator::Operator(NLNode* node) {
		if (node->value == NL_OPERATOR_ASSIGN) {
			return Assign(node);
		} else {
			NLObject *a = RunNode(node->childs[0]),
				*b = RunNode(node->childs[1]);

			string type = a->operator[]("type")->value;

			vector<NLObject*> args(0);

			args.push_back(a);
			args.push_back(b);

			string operatorString = "operator" + node->value;

			if (classes[type]->IsFunctionExists(operatorString)) {
				NLObject* funct = classes[type]->operator[](operatorString);

				return CallFunction(funct, args);
			} else {
				return NLObject::CreateError(&globals, "No " + operatorString + " defned in " + type);
			}
		}
	}

	NLObject* NLInterpritator::IfStatement(NLNode* node) {
		bool doit = RunNode(node->childs[0])->ToCBool();

		if (doit) {
			for (unsigned int i = 0; i < node->childs[1]->childs.size(); i++) {
				NLObject* o = RunNode(node->childs[1]->childs[i]);
				if (returned) {
					return o;
				}

				if (node->childs[1]->childs[i]->type == NLNode::Break) {
					break;
				}
			}
		} else if (node->childs.size() > 2) {
			for (unsigned int i = 0; i < node->childs[2]->childs.size(); i++) {
				NLObject* o = RunNode(node->childs[2]->childs[i]);
				if (returned) {
					return o;
				}

				if (node->childs[2]->childs[i]->type == NLNode::Break) {
					break;
				}
			}
		}

		return new NLObject();
	}
	
	NLObject* NLInterpritator::WhileStatement(NLNode* node) {
		while (RunNode(node->childs[0])->ToCBool()) {
			for (unsigned int i = 0; i < node->childs[1]->childs.size(); i++) {
				NLObject* o = RunNode(node->childs[1]->childs[i]);
				if (returned) {
					return o;
				}

				if (node->childs[1]->childs[i]->type == NLNode::Break) {
					break;
				}
			}
		}

		return new NLObject();
	}

	NLObject* NLInterpritator::CallFunction(NLNode* node) {
		string name = GetName(node);

		NLObject* function = new NLObject(&globals, true);

		if (currentEnviroment->IsFunctionExists(name)) {
			function = currentEnviroment->operator[](name);
		} else if (globals.IsFunctionExists(name)) {
			function = globals[name];
		} else {
			return NLObject::CreateError(&globals, "Function doesn't exists (" + name + ")");
		}

		vector<NLObject*> args(0);
		for (unsigned int i = 0; i < node->childs.size(); i++) {
			args.push_back(RunNode(node->childs[i]));
		}

		return CallFunction(function, args);
	}

	NLObject* NLInterpritator::CallFunction(NLObject* function, vector<NLObject*> args) {
		if (function->isFunctionBinded) {
			return function->CallBinded(args);
		} else {
			NLObject* oldEnviropment = currentEnviroment;
			currentEnviroment = function;
			
			RunNode(function->functionDefenition.childs[0]);

			for (unsigned int i = 0; i < args.size(); i++) {
				if (i >= function->functionDefenition.childs[0]->childs.size()) {
					break;
				}
				string name = GetName(function->functionDefenition.childs[0]->childs[i]);
				currentEnviroment->operator[](name) = args[i];
			}

			NLObject* res = new NLObject();

			for (unsigned int i = 1; i < function->functionDefenition.childs.size(); i++) {
				NLObject* o = RunNode(function->functionDefenition.childs[i]);
				if (returned) {
					res = o;
					goto __FunctionEnd__;
				}
			}
			__FunctionEnd__:
			currentEnviroment = oldEnviropment;

			return res;
		}
	}

	NLObject* NLInterpritator::Assign(NLNode* node) {
		if (node->childs.size() != 2) {
			return new NLObject();
		}

		string name = GetName(node->childs[0]);

		if (name.empty()) {
			return new NLObject();
		}


		if (currentEnviroment->IsVariableExists(name)) {
			return (currentEnviroment->operator[](name) = RunNode(node->childs[1]));
		} else if (globals.IsVariableExists(name)) {
			return globals[name] = RunNode(node->childs[1]);
		} else {
			return NLObject::CreateError(&globals, "Variable doesn't exists(" + name + ")");
		}

		return new NLObject();
	}

	string NLInterpritator::GetName(NLNode* node) {
		if (node->type == NLNode::Identifer || node->type == NLNode::Call ||
			node->type == NLNode::Defenition || node->type == NLNode::FunctionDefenition) {
			return node->value;
		} else {
			return "";
		}
	}
}