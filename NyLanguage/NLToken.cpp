#include <string>

#include "NL.h"
#include "NLToken.h"

namespace Ny {
	NLToken::NLToken() {
		type = NL_TOKEN_NONE;
		value = "";
	}

	NLToken::NLToken(const unsigned int type, string value) {
		this->type = type;
		this->value = value;
	}

	NLToken::~NLToken() {}

	unsigned int NLToken::GetType() {
		return type;
	}

	unsigned int NLToken::Special() {
		if (type != NL_TOKEN_WORD) {
			return 0;
		} else {
			if (value == NL_SYMBOL_DEFENITION) {
				return NL_NUM_DEFENNITION;
			} else if (value == NL_SYMBOL_RETURN) {
				return NL_NUM_RETURN;
			} else if (value == NL_SYMBOL_BREAK) {
				return NL_NUM_BREAK;
			} else if (value == NL_SYMBOL_IF) {
				return NL_NUM_IF;
			} else if(value == NL_SYMBOL_ELSE) {
				return NL_NUM_ELSE;
			} else if (value == NL_SYMBOL_WHILE) {
				return NL_NUM_WHILE;
			}
		}

		return 0;
	}

	string NLToken::GetValue() {
		return value;
	}

	//TODO: Create one method for all
	NLNode* NLToken::GetIndentiferNode() {
		NLNode* identifer = new NLNode();

		identifer->type = NLNode::Identifer;
		identifer->value = value;

		return identifer;
	}

	NLNode* NLToken::GetValueNode() {
		NLNode* identifer = new NLNode();

		identifer->type = NLNode::Value;
		switch (type) {
			case NL_TOKEN_DIGIT:
				identifer->subtype = NL_TYPE_INTEGER;
				break;

			case NL_TOKEN_STRING:
				identifer->subtype = NL_TYPE_STRING;
				break;
		}
		identifer->value = value;

		return identifer;
	}

	NLNode* NLToken::GetOperatorNode() {
		NLNode* identifer = new NLNode();

		identifer->type = NLNode::Operator;
		identifer->value = value;

		return identifer;
	}

	NLNode* NLToken::GetFunctionCallNode() {
		NLNode* identifer = new NLNode();

		identifer->type = NLNode::Call;
		identifer->value = value;

		return identifer;
	}

	NLNode* NLToken::GetDefenitionNode() {
		NLNode* defenition = new NLNode();

		defenition->type = NLNode::Defenition;
		defenition->value = value;

		return defenition;
	}
}