#pragma once
#include "NL.h"
#include "NLObject.h"
#include "NLNode.h"

namespace Ny {
	class NLInterpritator {
	public:
		bool showTree, returned;

		NLInterpritator();
		~NLInterpritator();
		int Run(const char* filename);

	private:
		void RunTree(NLNode program);
		
		NLObject* RunNode(NLNode* node);
		NLObject* Operator(NLNode* node);
		NLObject* Assign(NLNode* node);
		NLObject* IfStatement(NLNode* node);
		NLObject* WhileStatement(NLNode* node);
		NLObject* CallFunction(NLNode* node);
		NLObject* CallFunction(NLObject* function, vector<NLObject*> args);

		string GetName(NLNode* node);
	};
}

