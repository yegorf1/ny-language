#pragma once
#include <string>

#include "NL.h"
#include "NLNode.h"

using namespace std;

namespace Ny {
	const unsigned int
		NL_TOKEN_NONE = 0u,
		NL_TOKEN_WORD = 1u,
		NL_TOKEN_DIGIT = 2u,
		NL_TOKEN_SYMBOL = 3u,
		NL_TOKEN_STRING = 4u,
		NL_TOKEN_NEWLINE = 5u;

	class NLToken {
	public:
		NLToken();
		NLToken(const unsigned int type, string value);
		~NLToken();

		unsigned int GetType();
		unsigned int Special();
		string GetValue();

		NLNode* GetIndentiferNode();
		NLNode* GetValueNode();
		NLNode* GetOperatorNode();
		NLNode* GetFunctionCallNode();
		NLNode* GetDefenitionNode();

	private:
		unsigned int type;
		string value;
	};
}

