#pragma once
#include <vector>
#include <queue>
#include "NL.h"
#include "NLToken.h"
#include "NLNode.h"

namespace Ny {
	class NLSyntaxTreeBuilder {
	private:
		unsigned int currentToken, currentDepth;
		vector<NLToken> tokens;
		bool endedWithBracket, endedWithComa;

		NLNode* currentNode;

	public:
		NLNode program;


		NLSyntaxTreeBuilder();
		NLSyntaxTreeBuilder(vector<NLToken> tokens);
		~NLSyntaxTreeBuilder();


		int GenerateTree();


		queue<NLToken> ReadNextLine();
		NLToken GetCurrentToken();
		NLToken GetNextToken();
		NLToken GetToken(unsigned int token);

		NLToken GetNextWord();


		void WorkWithBlock(int depth);
		bool WorkWithLine(queue<NLToken> line);

		int Define(queue<NLToken> defenition);
		int Operator(NLToken op, NLNode* left, NLNode* right);

		NLNode* GetExpressionNode(queue<NLToken>& exp, bool isFuncArg);
		NLNode* GetValueNode(queue<NLToken>& exp);
		NLNode* GetFunctionArgumentsNode(queue<NLToken>& exp);

		bool isfunction(const queue<NLToken>& q);
		int depth(queue<NLToken> line);
	};
}

