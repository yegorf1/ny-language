#pragma once
#include "NL.h"
#include "NLObject.h"
#include "NLInterpritator.h"

#include <string>
#include <iostream>

using namespace std;

namespace Ny {
	class NLBinds {
	public:
		static NLObject *globals;


		static NLObject* import(vector<NLObject*> args) {
			NLInterpritator* inter = new NLInterpritator();

			for (unsigned int i = 0; i < args.size(); i++) {
				if (args[i]->type == NL_TYPE_STRING) {

					if (exists(args[i]->value)) {
						inter->Run(args[i]->value.c_str());
					} else if (exists("std/" + args[i]->value)) {
						string file = "std/" + args[i]->value;
						inter->Run(file.c_str());
					}
				}
			}

			return new NLObject();
		}

		static NLObject* import_io(vector<NLObject*> args) {
			NLObject::NLBind b;

			b = &NLBinds::print;
			globals->operator[]("print") = NLObject::CreateFunction(globals, b, "print");

			b = &NLBinds::println;
			globals->operator[]("println") = NLObject::CreateFunction(globals, b, "println");

			b = &NLBinds::read;
			globals->operator[]("read") = NLObject::CreateFunction(globals, b, "read");

			b = &NLBinds::readln;
			globals->operator[]("readln") = NLObject::CreateFunction(globals, b, "readln");

			return new NLObject();
		}

		//IO
		static NLObject* print(vector<NLObject*> args) {
			for (unsigned int i = 0; i < args.size(); i++) {
				cout << args[i]->value;
			}

			return new NLObject();
		}

		static NLObject* println(vector<NLObject*> args) {
			print(args);
			cout << endl;

			return new NLObject();
		}

		static NLObject* read(vector<NLObject*> args) {
			string str;
			getline(cin, str);

			return NLObject::CreateString(globals, str);
		}

		static NLObject* readln(vector<NLObject*> args) {
			char a;
			cin >> a;

			string str = "";
			str += a;

			return NLObject::CreateString(globals, str);
		}
	};
}

