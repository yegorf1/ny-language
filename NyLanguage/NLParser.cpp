#include <string>
#include <fstream>
#include <vector>
#include <streambuf>
#include <iostream>

#include "NLToken.h"
#include "NLParser.h"

using namespace std;

namespace Ny {
	NLParser::NLParser(const char* filename) {
		this->file = filename;
	}

	NLParser::~NLParser() {}

	string NLParser::ReadString(string &line, unsigned int &begin) {
		string res = "";

		begin++;

		for (; begin < line.size(); begin++) {
			char c = line[begin];

			if (c == '\\') {
				begin++;
				res += spec(line[begin]);
			} else if (isstringbegin(c)) {
				return res;
			} else if (isnewline(c) && c != ' ') {
				//TODO: Create throw
			} else {
				res += c;
			}
		}

		return res;
	}

	int NLParser::GetTokens(vector<NLToken> &tokens) {
		string value("");
		unsigned int type = NL_TOKEN_NONE;

		tokens.clear();

		ifstream t;
		t.open(file);
		string line;



		getline(t, line);
		while (t) {
			bool stillNoCode = true;
			for (unsigned int i = 0; i < line.size(); i++) {
				char c = line[i];

				if (c == '#') {
					break;
				} if (isspace(c)) {
					if (type) {
						tokens.push_back(NLToken(type, value));

						type = NL_TOKEN_NONE;
						value = "";
					}
					if (stillNoCode) {
						tokens.push_back(NLToken(NL_TOKEN_SYMBOL, "\t"));
					}
				} else {
					stillNoCode = false;
					if (isalpha(c)) {
						switch (type) {
							case NL_TOKEN_NONE:
								type = NL_TOKEN_WORD;
								value = c;
								break;

							case NL_TOKEN_WORD:
								value += c;
								break;

							case NL_TOKEN_DIGIT:
								return NL_SYNTAX_UNEXPECTED_SYMBOL;

							default:
								break;
						}
					} else if (isdigit(c)) {
						if (type == NL_TOKEN_DIGIT || type == NL_TOKEN_WORD) {
							value += c;
						} else if (type == NL_TOKEN_NONE) {
							value += c;
							type = NL_TOKEN_DIGIT;
						} else {
							//TODO: Error
						}
					} else if (ispunct(c)) {
						if (c == '_') {
							value += c;
						} else {
							if (type) {
								tokens.push_back(NLToken(type, value));

								type = NL_TOKEN_NONE;
								value = "";
							}

							if (isstringbegin(c)) {
								value.assign(ReadString(line, i));
								tokens.push_back(NLToken(NL_TOKEN_STRING, value));
								type = NL_TOKEN_NONE;
								value = "";
							} else {
								value += c;
								tokens.push_back(NLToken(NL_TOKEN_SYMBOL, value));
								type = NL_TOKEN_NONE;
								value = "";
							}
						}
					}
				}
			}
			if (type) {
				tokens.push_back(NLToken(type, value));

				type = NL_TOKEN_NONE;
				value = "";
			}

			tokens.push_back(NLToken(NL_TOKEN_NEWLINE, "NEWLINE"));

			getline(t, line);
		}

		t.close();

		return 0;
	}
}