#pragma once
#include "NLObject.h"
#include <vector>
#include <string>

namespace Ny {
	class NLInteger {
	public:
		static NLObject* Add(vector<NLObject*> args);
		static NLObject* Sub(vector<NLObject*> args);
		static NLObject* Multiply(vector<NLObject*> args);
		static NLObject* IsBigger(vector<NLObject*> args);
		static NLObject* IsLess(vector<NLObject*> args);
		static NLObject* IsEquals(vector<NLObject*> args);

		static NLObject* isNegative(vector<NLObject*> args);
		static NLObject* abs(vector<NLObject*> args);

	private:
		static bool isNegative(string value);
		static string abs(string value);
	};
}

