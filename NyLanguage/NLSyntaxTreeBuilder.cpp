#include <stack>

#include "NLSyntaxTreeBuilder.h"
#include "NL.h"	

using namespace std; //TODO: Remove

namespace Ny {
	NLSyntaxTreeBuilder::NLSyntaxTreeBuilder() {
		program = NLNode();
		program.type = NLNode::Program;

		currentNode = &program;
	}

	NLSyntaxTreeBuilder::NLSyntaxTreeBuilder(vector<NLToken> tokens) : NLSyntaxTreeBuilder() {
		this->tokens = vector<NLToken>(tokens);
	}

	NLSyntaxTreeBuilder::~NLSyntaxTreeBuilder() {}

	//TODO: Replace with WorkWithBlock();
	int NLSyntaxTreeBuilder::GenerateTree() {
		WorkWithBlock(0);

		return 0;
	}

	queue<NLToken> NLSyntaxTreeBuilder::ReadNextLine() {
		queue<NLToken> line;

		do {
			line.push(GetCurrentToken());
			currentToken++;
		} while (GetToken(currentToken - 1).GetType() != NL_TOKEN_NEWLINE);


		return line;
	}

	NLToken NLSyntaxTreeBuilder::GetCurrentToken() {
		return GetToken(currentToken);
	}

	NLToken NLSyntaxTreeBuilder::GetNextToken() {
		return GetToken(currentToken + 1);
	}

	NLToken NLSyntaxTreeBuilder::GetToken(unsigned int token) {
		if (token < 0 || token >= tokens.size()) {
			return NLToken(NL_TOKEN_NONE, "");
		} else {
			return tokens[token];
		}
	}

	NLToken NLSyntaxTreeBuilder::GetNextWord() {
		for (int i = currentToken; i < tokens.size(); i++) {
			NLToken t = GetToken(i);
			if (t.GetType() == NL_TOKEN_WORD) {
				return t;
			}
		}

		return NLToken(NL_TOKEN_NONE, "");
	}

	void NLSyntaxTreeBuilder::WorkWithBlock(int depth) {
		currentDepth = depth;
		queue<NLToken> line = ReadNextLine();
		bool still = WorkWithLine(line);

		while (tokens.size() - currentToken && still) {
			line = ReadNextLine();
			still = WorkWithLine(line);

			if (!still) {
				currentToken -= line.size();
			}
		}

		currentDepth--;
	}

	bool NLSyntaxTreeBuilder::WorkWithLine(queue<NLToken> line) {
		queue<NLToken> test = queue<NLToken>(line);

		//Check for block depth
		unsigned int d = depth(line);

		if (d < currentDepth) {
			return false;
		}

		for (unsigned int i = 0; i < d; i++) {
			line.pop();
		}

		NLToken first = line.front();

		if (first.GetType() != NL_TOKEN_NONE) {//If not empty
			unsigned int s = first.Special();
			switch (s) {
				case NL_NUM_DEFENNITION: {
					line.pop();
					Define(line);
					break;
				}
				
				case NL_NUM_RETURN: {
					line.pop();
					NLNode* returnNodeResult = GetExpressionNode(line, false);
					NLNode* returnNode = new NLNode();

					returnNode->type = NLNode::Return;
					returnNode->childs.push_back(returnNodeResult);

					currentNode->childs.push_back(returnNode);
					break;
				}

				case NL_NUM_BREAK: {
					line.pop();
					NLNode* breakNode = new NLNode();

					breakNode->type = NLNode::Break;

					currentNode->childs.push_back(breakNode);
					break;
				}

				case NL_NUM_WHILE:
				case NL_NUM_IF: {
					line.pop();
					NLNode* resNode = new NLNode();

					resNode->type = s == NL_NUM_IF ? NLNode::If : NLNode::While;

					NLNode  *statement  = GetExpressionNode(line, false),
							*body		= new NLNode();
					body->type = NLNode::List;

					resNode->childs.push_back(statement);

					NLNode* old = currentNode;
					currentNode = body;

					WorkWithBlock(currentDepth + 1);

					currentNode = old;

					resNode->childs.push_back(body);

					if (s == NL_NUM_IF && GetNextWord().Special() == NL_NUM_ELSE) {
						ReadNextLine(); //Here was else
						NLNode* elseNode = new NLNode();
						elseNode->type = NLNode::List;

						old = currentNode;
						currentNode = elseNode;

						WorkWithBlock(currentDepth + 1);

						currentNode = old;

						resNode->childs.push_back(elseNode);
					}

					currentNode->childs.push_back(resNode);
					break;
				}

				default: {
					NLNode* newNode = GetExpressionNode(line, false);
					if (newNode->type != NLNode::None) {
						currentNode->childs.push_back(newNode);
					}
					break;
				}
			}
		}

		return true;
	}

	int NLSyntaxTreeBuilder::Define(queue<NLToken> defenition) {
		if (!defenition.size()) {
			//TODO: Error
		}

		NLToken name = defenition.front();
		defenition.pop();

		if (name.GetType() != NL_TOKEN_WORD) {
			//TODO: Errror
		}

		bool isFunc = isfunction(defenition);

		NLNode* defenitionNode = new NLNode();

		defenitionNode->value = name.GetValue();
		defenitionNode->type = isFunc ? NLNode::FunctionDefenition : NLNode::Defenition;

		if (isFunc) {
			defenition.pop();
			NLNode* argsNode = GetFunctionArgumentsNode(defenition);

			defenitionNode->childs.push_back(argsNode);

			NLNode* old = currentNode;
			currentNode = defenitionNode;

			WorkWithBlock(currentDepth + 1);

			currentNode = old;
		}

		currentNode->childs.push_back(defenitionNode);

		if (defenition.size()) {
			NLToken next = defenition.front();
			defenition.pop();
			bool coma = false;

			if (next.GetType() == NL_TOKEN_SYMBOL && next.GetValue() == NL_OPERATOR_ASSIGN) {
				Operator(next, name.GetIndentiferNode(), GetExpressionNode(defenition, true));
				coma = endedWithComa;
			} else if (next.GetType() == NL_TOKEN_SYMBOL && next.GetValue() == ",") {
				Define(defenition);
			} else {
				//TODO: Error
			}

			if (defenition.size()) {
				NLToken next = defenition.front();
				if (coma || (next.GetType() == NL_TOKEN_SYMBOL && next.GetValue() == ",")) {
					Define(defenition);
				}
			}
		}

		return 0;
	}

	int NLSyntaxTreeBuilder::Operator(NLToken op, NLNode* left, NLNode* right) {
		NLNode* operatorNode = new NLNode();

		operatorNode->type = NLNode::Operator;
		operatorNode->value = op.GetValue();
		operatorNode->childs.push_back(left);
		operatorNode->childs.push_back(right);

		currentNode->childs.push_back(operatorNode);

		return 0;
	}

	NLNode* NLSyntaxTreeBuilder::GetExpressionNode(queue<NLToken>& exp, bool isFuncArg) {
		endedWithBracket = endedWithComa = false;
		stack<NLNode*> operations, output;

		NLNode* nextToken = GetValueNode(exp);

		while (nextToken->type != NLNode::None) {
			if (nextToken->type == NLNode::Operator && nextToken->value == ")") {
				NLNode* tok = new NLNode();
				if (operations.size()) {
					tok = operations.top();
					operations.pop();
					while (tok->value != "(") {
						output.push(tok);

						if (operations.size()) {
							tok = operations.top();
							operations.pop();
						} else {
							break;
						}
					}
				}

				if (tok->value != "(") {
					if (isFuncArg) {
						endedWithBracket = true;
						break;
					}
					//TODO: Error
				} else {

				}
			} else if (nextToken->IsOperand()) {
				output.push(nextToken);
			} else if (nextToken->type == NLNode::Operator && nextToken->value == "(") {
				operations.push(nextToken);
			} else if (nextToken->OperatorPriority()) {
				if (!operations.size()) {
					operations.push(nextToken);
				} else {
					//TODO: To long I think =/
					while (operations.size() && operations.top()->OperatorPriority() >= nextToken->OperatorPriority()) {
						output.push(operations.top());
						operations.pop();
					}

					operations.push(nextToken);
				}
			} else if (nextToken->type == NLNode::Operator && nextToken->value == ",") {
				if (isFuncArg) {
					endedWithComa = true;
					break;
				} else {
					//TODO: Error
				}
			} else {
				//TODO: Error
			}

			nextToken = GetValueNode(exp);
		}

		while (operations.size()) {
			output.push(operations.top());
			operations.pop();
		}

		//Reverse
		NLNode* item;
		stack<NLNode*> tmpStack;

		while (output.size()) {
			item = output.top();
			output.pop();
			tmpStack.push(item);
		}

		output = tmpStack;

		stack<NLNode*> endStack;

		while (output.size()) {
			NLNode* e = output.top();
			output.pop();

			if (e->OperatorPriority()) {
				NLNode *left, *right;
				left = endStack.top();
				endStack.pop();
				right = endStack.top();
				endStack.pop();

				if (!e->IsLeftAsociated()) {
					swap(left, right);
				}

				e->childs.push_back(left);
				e->childs.push_back(right);
			}

			endStack.push(e);
		}

		//Result
		NLNode* res;

		if (endStack.size()) {
			res = endStack.top();

			endStack.pop();
		} else {
			res = new NLNode();
		}

		return res;
	}

	NLNode* NLSyntaxTreeBuilder::GetValueNode(queue<NLToken>& exp) {
		if (exp.size() > 0) {
			NLToken* t = new NLToken(exp.front());
			exp.pop();

			switch (t->GetType()) {
				default:
					//TODO: Error
					break;

				case NL_TOKEN_DIGIT:
				case NL_TOKEN_STRING:
					return t->GetValueNode();

				case NL_TOKEN_WORD:
					if (isfunction(exp)) {
						NLNode* func = t->GetFunctionCallNode();

						exp.pop(); //Here was '('

						do {
							NLNode* arg = GetExpressionNode(exp, true);

							if (arg->type != NLNode::None) {
								func->childs.push_back(arg);
							}
						} while (!endedWithBracket);

						return func;
					} else {
						return t->GetIndentiferNode();
					}
					break;

				case NL_TOKEN_SYMBOL:
					return t->GetOperatorNode();

				case NL_TOKEN_NEWLINE:
					goto __None__;
			}
		} else {
		__None__:
			return new NLNode();
		}

		//TODO: Remove
		return new NLNode();
	}

	NLNode* NLSyntaxTreeBuilder::GetFunctionArgumentsNode(queue<NLToken>& exp) {
		NLNode* res = new NLNode();

		res->type = NLNode::List;

		bool endedWithBracket = false;

		while (!endedWithBracket) {
			if (exp.size()) {
				NLToken tok = exp.front();
				exp.pop();

				if (tok.GetType() == NL_TOKEN_WORD) {
					res->childs.push_back(tok.GetDefenitionNode());
				} else if (tok.GetType() == NL_TOKEN_SYMBOL && tok.GetValue() == ")") {
					endedWithBracket = true;
				} else {
					//TODO: Error
				}

				if (exp.size() && !endedWithBracket) {
					tok = exp.front();
					exp.pop();

					if (tok.GetType() == NL_TOKEN_SYMBOL) {
						if (tok.GetValue() == ")") {
							endedWithBracket = true;
						} else if (tok.GetValue() != ",") {
							//TODO: Error
						}
					} else {
						//TODO: Error
					}

				} else {
					//TODO: Error
				}
			} else {
				//TODO: Error
			}
		}

		if (exp.size() && exp.front().GetType() == NL_TOKEN_NEWLINE) {
			exp.pop();
		} //No error in else. Really. Can be: def foo(a) a = a + 1

		return res;
	}

	inline bool NLSyntaxTreeBuilder::isfunction(const queue<NLToken>& q) {
		if (q.size()) {
			NLToken t = q.front();

			return t.GetType() == NL_TOKEN_SYMBOL && t.GetValue() == "(";
		}

		return false;
	}

	inline int NLSyntaxTreeBuilder::depth(queue<NLToken> line) {
		int res = 0;

		while (line.front().GetType() == NL_TOKEN_SYMBOL && line.front().GetValue() == "\t") {
			line.pop();
			res++;
		}

		return res;
	}
}
