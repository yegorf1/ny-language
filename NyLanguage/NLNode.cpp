#include <iostream>
#include "NLNode.h"

namespace Ny {
	NLNode::NLNode() {
		type = None;
		subtype = "";
	}

	NLNode::~NLNode() {}


	void NLNode::PrintToCout(int depth) {
		for (int i = 0; i < depth; i++) {
			cout << "-";
		}

		cout << "+";

		cout << TypeToString();

		if (!value.empty()) {
			cout << "=\'" << value << '\'';
		}

		cout << endl;

		for (unsigned int i = 0; i < childs.size(); i++) {
			childs[i]->PrintToCout(depth + 1);
		}
	}
}