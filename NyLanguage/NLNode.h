#pragma once
#include <vector>
#include <string>

#include "NL.h"

using namespace std;

namespace Ny {

	class NLNode {
	public:
		enum NLENodeType {
			None,
			Program,
			Operator,
			Call,
			Identifer,
			Value,
			Defenition,
			FunctionDefenition,
			List,
			Return,
			Break,
			If,
			While
		} type;

		vector<NLNode*> childs;
		string value, subtype;

		NLNode();
		~NLNode();

		void PrintToCout(int depth);

		inline bool IsOperand() {
			return type == Call || type == Value || type == Identifer;
		}

		inline int OperatorPriority() {
			if (value == NL_OPERATOR_ASSIGN) {
				return -10000;
			} else if (value == NL_OPERATOR_BIGGER || value == NL_OPERATOR_LESS) {
				return 5;
			} else if (value == NL_OPERATOR_POW) {
				return 4;
			} else if (value == NL_OPERATOR_MULTIPLY || value == NL_OPERATOR_DEVIDE) {
				return 3;
			} else if (value == NL_OPERATOR_ADD || value == NL_OPERATOR_SUB) {
				return 2;
			} else if (value == "(") {
				return 1;
			} else {
				return 0;
			}
		}

		inline bool IsLeftAsociated() {
			return
				value == NL_OPERATOR_POW;
		}
#ifdef WINDOWS
		inline char* TypeToString() {
			char * EnumStrings[] = { "None", "Program", "Operator", "Call", "Identifer", "Value", "Defenition", "FunctionDefenition", "List", "Return", "Break", "If", "While" };
			return EnumStrings[type];
		}
#else
		inline string TypeToString() {
			string EnumStrings[] = { "None", "Program", "Operator", "Call", "Identifer", "Value", "Defenition", "FunctionDefenition", "List", "Return", "Break", "If", "While" };
			return EnumStrings[type];
		}
#endif
	};
}

