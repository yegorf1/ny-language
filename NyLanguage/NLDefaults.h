#pragma once
#include "NL.h"
#include "NLInterpriterGlobals.h"
#include "NLBinds.h"
#include "NLInteger.h"
#include "NLObject.h"


namespace Ny {
	class NLDefaults {
	private:
	public:
		static inline void CreateDefaults(NLObject& globals, NLObject& classes) {
			NLBinds::globals = &globals;

			CreateDefaultClasses(globals, classes);
			CreateDefaultVariables(globals, classes);
			CreateDefaultBinds(globals, classes);
		}

	private:
		static inline void CreateDefaultBinds(NLObject &globals, NLObject &classes) {
			NLObject::NLBind b;
			b = &NLBinds::import;
			globals["import"] = NLObject::CreateFunction(&globals, b, "import");

			b = &NLBinds::import_io;
			globals["import_io"] = NLObject::CreateFunction(&globals, b, "import_io");
		}

		static inline void CreateDefaultVariables(NLObject &globals, NLObject &classes) {
			globals["true"] = NLObject::CreateBool(&globals, true);
			globals["false"] = NLObject::CreateBool(&globals, false);

			globals["none"] = new NLObject(&globals);
			globals["null"] = new NLObject(&globals);
		}

		static inline void CreateDefaultClasses(NLObject &globals, NLObject &classes) {
			CreateInt(globals, classes);
			CreateString(globals, classes);
			CreateBoolean(globals, classes);
		}

		static inline void CreateInt(NLObject &globals, NLObject &classes) {
			NLObject* intClass = NLObject::CreateClass(&globals, NL_TYPE_INTEGER, vector<string>(0));
			classes[NL_TYPE_INTEGER] = intClass;

			NLObject::NLBind b = &NLInteger::Add;
			intClass->operator[]("operator+") = NLObject::CreateFunction(&globals, b, "operator+");

			b = &NLInteger::Sub;
			intClass->operator[]("operator-") = NLObject::CreateFunction(&globals, b, "operator-");

			b = &NLInteger::Multiply;
			intClass->operator[]("operator*") = NLObject::CreateFunction(&globals, b, "operator*");

			b = &NLInteger::IsBigger;
			intClass->operator[]("operator>") = NLObject::CreateFunction(&globals, b, "operator>");

			b = &NLInteger::IsLess;
			intClass->operator[]("operator<") = NLObject::CreateFunction(&globals, b, "operator<");

			b = &NLInteger::IsEquals;
			intClass->operator[]("operator==") = NLObject::CreateFunction(&globals, b, "operator==");
		}

		static inline void CreateString(NLObject &globals, NLObject &classes) {
		}

		static inline void CreateBoolean(NLObject &globals, NLObject &classes) {
		}
	};
}

