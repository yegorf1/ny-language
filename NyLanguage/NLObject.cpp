#include "NLObject.h"
#include "NLDefaults.h"

namespace Ny {
	NLObject::NLObject() {
		isFunc = false;

		functionDefenition = NLNode();

		value = "None";
		type = "None";
	}

	NLObject::NLObject(NLObject* globals) : NLObject(){
		this->globals = globals;
	}

	NLObject::NLObject(NLObject* globals, bool isFunc) : NLObject(globals) {
		this->isFunc = isFunc;
	}

	NLObject::~NLObject() {}

	NLObject* NLObject::CreateClass(NLObject* globals, string name, vector<string> parents) {
		NLObject* res = new NLObject(globals, false);

		res->value = name;

		res->fields["constructor"] = CreateFunction(globals, NLNode());
		res->fields["operator+"] = CreateFunction(globals, NLNode());
		res->fields["operator-"] = CreateFunction(globals, NLNode());
		res->fields["operator/"] = CreateFunction(globals, NLNode());
		res->fields["operator*"] = CreateFunction(globals, NLNode());
		res->fields["operator^"] = CreateFunction(globals, NLNode());
		res->fields["operator<"] = CreateFunction(globals, NLNode());
		res->fields["operator>"] = CreateFunction(globals, NLNode());

		return res;
	}

	NLObject* NLObject::CreateFunction(NLObject* globals, NLNode defenition) {
		NLObject* res = new NLObject(globals, true);

		res->isFunctionBinded = false;
		res->functionDefenition = defenition;

		res->value = defenition.value;

		res->fields["name"] = CreateString(globals, defenition.value);
		res->type = NL_TYPE_FUNCTION;

		return res;
	}

	NLObject* NLObject::CreateFunction(NLObject* globals, NLBind bind, string name) {
		NLObject* res = new NLObject(globals, true);

		res->isFunctionBinded = true;
		res->bind = bind;

		res->value = name;

		res->fields["name"] = CreateString(globals, name);
		res->type = NL_TYPE_FUNCTION;

		return res;
	}

	NLObject* NLObject::CreateInteger(NLObject* globals, string integer) {
		NLObject* res = new NLObject(globals, false);

		res->value = integer;
		res->type = NL_TYPE_INTEGER;

		return res;
	}

	NLObject* NLObject::CreateError(NLObject* globals, string error) {
		NLObject* res = new NLObject(globals, false);

		res->value = error;
		res->type = NL_TYPE_ERROR;

		return res;
	}

	NLObject* NLObject::CreateString(NLObject* globals, string str) {
		NLObject* res = new NLObject(globals, false);

		res->value = str;
		res->type = NL_TYPE_STRING;

		return res;
	}

	NLObject* NLObject::CreateBool(NLObject* globals, bool boolean) {
		NLObject* res = new NLObject(globals, false);

		res->value = boolean ? "true" : "false";
		res->type = NL_TYPE_BOOLEAN;

		return res;
	}

	NLObject* NLObject::PutNone(string name) {
		map<string, NLObject*>::iterator it = fields.begin();
		fields.insert(it, pair<string, NLObject*>(name, new NLObject()));

		return fields[name];
	}

	bool NLObject::IsVariableExists(string name) {
		return IsObjectExists(name) && !fields[name]->isFunc;
	}

	bool NLObject::IsFunctionExists(string name) {
		return IsObjectExists(name) && fields[name]->isFunc;
	}

	bool NLObject::IsObjectExists(string name) {
		return fields.find(name) != fields.end();
	}
}
